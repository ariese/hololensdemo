﻿using UnityEngine;
using HoloToolkitExtensions.Messaging;

public class SpeechCommandExecutor : MonoBehaviour
{
    public GameObject InfoScreen;

    public GameObject InfoScreenVoice;

    private bool toggle = false;
    private bool toggle2 = false;

    public FadeObjectInOut fade;

    public FadeObjectInOut fadeVoice;

    public void ToggleHelpScreen()
    {
        toggle = !toggle;

        if (toggle)
        {
            fade.FadeOut();
        }

        else
        {
            fade.FadeIn();
        }
    }

    public void ToggleVoiceScreen()
    {
        toggle2 = !toggle2;

        if (toggle2)
        {
            fadeVoice.FadeOut();
        }

        else
        {
            fadeVoice.FadeIn();
        }
    }
}
