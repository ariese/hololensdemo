﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tractors : MonoBehaviour
{
    [System.Serializable]
    public struct ObjectToCycleThrough
    {
        [Tooltip("The object to set")]
        public GameObject my_object;

        [Tooltip("Active at Start")]
        public bool active_at_start;
    };

    private int front_count = 1;

    private int back_count = 1;


    [Tooltip("An array of gameobjects to cycle through for the front attachments")]
    public ObjectToCycleThrough[] ObjectsToCycleThroughFront;

    [Tooltip("An array of gameobjects to cycle through for the back attachments")]
    public ObjectToCycleThrough[] ObjectsToCycleThroughBack;

    private GameObject Plow;
    private GameObject BrushGuard;
    private GameObject Spreader;
    private GameObject Sprayer;
    private GameObject CargoCarrier;


    public void Start()
    {
        for (int i = 0; i < ObjectsToCycleThroughFront.Length; i++)
        {
            ObjectsToCycleThroughFront[i].my_object.SetActive(ObjectsToCycleThroughFront[i].active_at_start);
        }

        for (int i = 0; i < ObjectsToCycleThroughBack.Length; i++)
        {
            ObjectsToCycleThroughBack[i].my_object.SetActive(ObjectsToCycleThroughBack[i].active_at_start);
        }

        SetItems();
    }

    public void SetItems()
    {
        Plow = ObjectsToCycleThroughFront[0].my_object;
        BrushGuard = ObjectsToCycleThroughFront[1].my_object;

        Spreader = ObjectsToCycleThroughBack[0].my_object;
        Sprayer = ObjectsToCycleThroughBack[1].my_object;
        CargoCarrier = ObjectsToCycleThroughBack[2].my_object;
    }

    /// <summary>
    /// If we want the on screen button, uncomment this. But that button really
    /// only is useful for the phone app, since you cannot see it on the hololens
    /// </summary>
    //private void OnGUI()
    //{
    //    if (GUI.Button(new Rect(10, 200, 200, 60), "Next"))
    //    {
    //        goToNextAttachment();
    //    }
    //}

    public void goToNextFrontAttachment()
    {
        if (front_count > ObjectsToCycleThroughFront.Length)
        {
            for (int x = 0; x < ObjectsToCycleThroughFront.Length; x++)
            {
                ObjectsToCycleThroughFront[x].my_object.SetActive(false);
            }

            front_count = 0;
        }

        for (int x = 0; x < ObjectsToCycleThroughFront.Length; x++)
        {
            if (x == front_count)
            {
                ObjectsToCycleThroughFront[x].my_object.SetActive(true);
            }

            else
            {
                ObjectsToCycleThroughFront[x].my_object.SetActive(false);
            }
        }

        front_count++;
    }

    public void goToNextBackAttachment()
    {
        if (back_count > ObjectsToCycleThroughBack.Length)
        {
            for (int x = 0; x < ObjectsToCycleThroughBack.Length; x++)
            {
                ObjectsToCycleThroughBack[x].my_object.SetActive(false);
            }

            back_count = 0;
        }

        for (int x = 0; x < ObjectsToCycleThroughBack.Length; x++)
        {
            if (x == back_count)
            {
                ObjectsToCycleThroughBack[x].my_object.SetActive(true);
            }

            else
            {
                ObjectsToCycleThroughBack[x].my_object.SetActive(false);
            }
        }

        back_count++;
    }

    public void SetPlowActive()
    {
        Plow.SetActive(true);

        BrushGuard.SetActive(false);
        //Spreader.SetActive(false);
        //Sprayer.SetActive(false);
        //CargoCarrier.SetActive(false);
    }

    public void SetBrushGuardActive()
    {
        BrushGuard.SetActive(true);

        Plow.SetActive(false);
        //Spreader.SetActive(false);
        //Sprayer.SetActive(false);
        //CargoCarrier.SetActive(false);
    }

    public void SetSpreaderActive()
    {
        Spreader.SetActive(true);

        //Plow.SetActive(false);
        //BrushGuard.SetActive(false);
        Sprayer.SetActive(false);
        CargoCarrier.SetActive(false);
    }

    public void SetSprayerActive()
    {
        Sprayer.SetActive(true);

        //Plow.SetActive(false);
        //BrushGuard.SetActive(false);
        Spreader.SetActive(false);
        CargoCarrier.SetActive(false);
    }

    public void SetCargoCarrierActive()
    {
        CargoCarrier.SetActive(true);

        //Plow.SetActive(false);
        //BrushGuard.SetActive(false);
        Spreader.SetActive(false);
        Sprayer.SetActive(false);
    }



}