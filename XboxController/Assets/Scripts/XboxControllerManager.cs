﻿using HoloToolkit.Unity.SpatialMapping;
using UnityEngine;
using HoloToolkit.Unity.InputModule;

public class XboxControllerManager : MonoBehaviour
{
    private bool toggleMapping = false;
    private bool toggleRigidbody = false;
    private bool toggleMenu = false;

    private bool toggleActive = true;

    public GameObject hologram;

    public GameObject screen;

    [Tooltip("Fade Object In Out Script goes here")]
    public FadeObjectInOut fade;

    [Tooltip("Gaze Manager")]
    public GazeManager gazeManager;

    public Tractors tractors;

    public GameObject tractorModel;

    private void Start()
    {
        //hologram = GameObject.Find("Cube");
        //screen = GameObject.Find("InfoScreen");
    }

    private void Update()
    {

        if (Input.GetButtonDown("ButtonX"))
        {
            toggleMapping = !toggleMapping;
            SpatialMappingManager.Instance.DrawVisualMeshes = toggleMapping;

        }

        if (Input.GetButtonDown("ButtonY"))
        {
            //hologram.GetComponent<Renderer>().material.color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
            toggleActive = !toggleActive;
            tractorModel.SetActive(toggleActive);
        }

        if (Input.GetButtonDown("LeftBumper"))
        {
            //hologram.transform.position = new Vector3(0, 0, 2);
            //hologram.transform.position = gazeManager.HitPosition;
            tractors.goToNextBackAttachment();
        }

        if (Input.GetButtonDown("RightBumper"))
        {
            tractors.goToNextFrontAttachment();
        }

        //if (Input.GetButtonDown("RightBumper"))
        //{
        //    toggleRigidbody = !toggleRigidbody;
        //    hologram.GetComponent<Rigidbody>().useGravity = toggleRigidbody;
        //}

        if (Input.GetAxis("DpadVertical") > 0.0f)
        { 
            hologram.gameObject.transform.localScale += new Vector3(0.005f, 0.005f, 0.005f);
        }

        if (Input.GetAxis("DpadVertical") < 0.0f)
        {
            if (hologram.gameObject.transform.localScale.x >= 0.005f &&
                hologram.gameObject.transform.localScale.y >= 0.005f &&
                hologram.gameObject.transform.localScale.z >= 0.005f)
            {
                hologram.gameObject.transform.localScale -= new Vector3(0.005f, 0.005f, 0.005f);
            }
        }

        if (Input.GetButtonDown("Start"))
        {
            toggleMenu = !toggleMenu;

            //for (int i = 0; i < screen.transform.childCount; i++)
            //{
            //    screen.transform.GetChild(i).gameObject.SetActive(toggleMenu);
            //}


            //for (int i = 255; i >= 0; i--)
            //{
            //    for (int x = 0; x < screen.transform.childCount; x++)
            //    {
            //        Color color = screen.transform.GetChild(x).gameObject.GetComponent<Renderer>().material.color;
            //        color.a = i;
            //        screen.transform.GetChild(x).gameObject.GetComponent<Renderer>().material.color = color;
            //    }


            //}   

            if (toggleMenu)
            {
                fade.FadeOut();
            }

            else
            {
                fade.FadeIn();
            }
            
        }

        if (Input.GetButtonDown("Select"))
        {
            hologram.transform.position = new Vector3(0, 0, 2);
            hologram.transform.position = gazeManager.HitPosition;
            hologram.transform.rotation = new Quaternion(0, 0, 0, 0);
        }

    }
}