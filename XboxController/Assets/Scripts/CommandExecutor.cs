﻿using UnityEngine;
using HoloToolkitExtensions.Messaging;

public class CommandExecutor : MonoBehaviour
{
    public void OpenHelpScreen()
    {
        Messenger.Instance.Broadcast(new ShowHelpMessage());
    }
}
