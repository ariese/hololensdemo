﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TapNext : MonoBehaviour, IInputClickHandler
{
    public Tractors tractors;

    private bool front;

 

    // Use this for initialization
    void Start()
    {
        if (gameObject.name == "HitboxFront")
        {
            front = true;
        }

        else if (gameObject.name == "HitboxBack")
        {
            front = false;
        }

        else
        {
            throw new Exception();
        }

    }


    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (front)
        {
            tractors.goToNextFrontAttachment();
        }

        else
        {
            tractors.goToNextBackAttachment();
        }     
    }

}
