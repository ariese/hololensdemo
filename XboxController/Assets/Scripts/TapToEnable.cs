﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TapToEnable : MonoBehaviour, IInputClickHandler
{
    private bool toggle;

   // private MeshRenderer meshRenderer;

	// Use this for initialization
	void Start ()
    {
        //meshRenderer = gameObject.GetComponent<MeshRenderer>(); 
        toggle = true;
	}
	
    public void OnInputClicked(InputClickedEventData eventData)
    {
        toggle = !toggle;

        Component[] renderers = gameObject.GetComponentsInChildren(typeof(Renderer));
        foreach (Renderer renderer in renderers)
        {
            renderer.enabled = toggle;
        }
        //GetComponent<Renderer>().enabled = toggle;
        //gameObject.SetActive(toggle);   
    }
}
