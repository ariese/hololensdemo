﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ToggleGlow : MonoBehaviour
{
    [Tooltip("Material to set the outline")]
    public Material OutlineMaterial;

    [Tooltip("Put the object with Gaze Manager script on it")]
    public GameObject InputManager;

    public Material EmptyOutline;

    private Material[] materials;

    private GazeManager gaze_manager;

    private Component[] components;

    // Use this for initialization
    void Start()
    {
        //Debug.Log("My name is " + gameObject.name);
        InputManager = GameObject.Find("InputManager");

        gaze_manager = InputManager.GetComponent<GazeManager>();

        // Initialize the renderers array based on the number of components we have 
        components = gameObject.GetComponentsInChildren<Renderer>();

        materials = new Material[2];
    }

    /**
     * When the object comes into focus, we want it to "light up". That way, the user will know
     *  that is is interactable.
     */
    public void Update()
    {
        if (gaze_manager.HitObject.name.Equals(gameObject.name))
        {
            // Set the second material for each renderer to the outline material we chose
            foreach (Renderer rend in components)
            {
                materials[0] = rend.sharedMaterials[0];
                materials[1] = OutlineMaterial;
                rend.sharedMaterials = materials;
            }
        }

        else
        {
            // Set the second material for each renderer to the outline material we chose
            foreach (Renderer rend in components)
            {
                materials[0] = rend.sharedMaterials[0];
                materials[1] = EmptyOutline;
                rend.sharedMaterials = materials;
            }
        }
    }
}
