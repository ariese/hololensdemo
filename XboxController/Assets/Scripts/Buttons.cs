﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour
{
    [Tooltip("First Game Object")]
    public GameObject my_object_1;

    [Tooltip("Second Game Object")]
    public GameObject my_object_2;

    [Tooltip("Third Game Object")]
    public GameObject my_object_3;

    private bool knight_on = false;
    private bool samurai_on = false;
    private bool lord_on = false;

    private int count = 0;
    private bool my_button;


    private void OnGUI()
    {
        if (GUI.Button(new Rect(10, 30, 200, 60), "Next"))
        {
            click();
        }   
    }

    public void click()
    {
        count++;

        if (count > 2)
        {
            count = 0;
        }

        if (count == 0)
        {
            knight_on = true;
            my_object_1.SetActive(knight_on);

            samurai_on = false;
            my_object_2.SetActive(samurai_on);

            lord_on = false;
            my_object_3.SetActive(lord_on);
        }

        else if (count == 1)
        {
            knight_on = false;
            my_object_1.SetActive(knight_on);

            samurai_on = true;
            my_object_2.SetActive(samurai_on);

            lord_on = false;
            my_object_3.SetActive(lord_on);
        }

        else if (count == 2)
        {
            knight_on = false;
            my_object_1.SetActive(knight_on);

            samurai_on = false;
            my_object_2.SetActive(samurai_on);

            lord_on = true;
            my_object_3.SetActive(lord_on);
        }
    }
}